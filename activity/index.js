// Activity:



/*
	1. Create a student grading system using an arrow function. The grade categories are as follows: Failed(74 and below), Beginner (75-80), Developing (81-85), Above Average (86-90), Advanced(91-100) 

	Sample output in the console: Congratulations! Your quarterly average is 85. You have received a Developing" mark.

*/

// Code here:
	
	let studentGrade = prompt("Please enter your grade: ");

	if (studentGrade < 75) {
		console.log("Unfortunately! Your quarterly average is " + studentGrade + " You have received a Failed mark");
	} else if (studentGrade >=75 && studentGrade <=80) {
		console.log("Congratulations! Your quarterly average is " + studentGrade + " You have received a Beginner mark");
	} else if (studentGrade > 80 && studentGrade <= 85 ) {
		console.log("Congratulations! Your quarterly average is " + studentGrade + " You have received a Developing mark");
	} else if (studentGrade > 85 && studentGrade <=90) {
		console.log("Congratulations! Your quarterly average is " + studentGrade + " You have received a Above Average mark");
	} else {
		console.log("Congratulations! Your quarterly average is " + studentGrade + " You have received a Advanced mark");
	}
/*
	2. Create an odd-even checker that will check which numbers from 1-300 are odd and which are even,

	Sample output in the console: 
		1 - odd
		2 - even
		3 - odd
		4 - even
		5 - odd
		etc.
*/

// Code here:
	
	let number = 301;

	for (x=1; x < number; x++){
		if (x % 2 == 0){
			console.log(x + " - Even")
		}
		else {
			console.log(x + " - Odd")
		}
	};


/*
	3. Create a an object named ""hero"" and input the details using promp(). Here are the details needed: heroName, origin, description, skills(object which will contain 3 uinique skills). Convert hero JS object to JSON data format and log the output in the console.

	Sample output in the console:
		{
		        "heroName": "Aldous",
		        "origin: "Minoan Empire,
		        "description: "A guard of the Minos Labyrinth who kept his pledge even after the kingdom's fall.,
		        "skills": {	
		                "skill1": "Soul Steal",
		                |"Skill2": "Explosion",
		                "Skill3": "Chase Fate"
		        }
		}
*/
	// Code here:
	let firstSkill = prompt("Enter your first skill: ");
	let secondSkill = prompt("Enter your second skill: ");
	let	thirdSkill = prompt("Enter your third skill: ");
	let hero = {
		name: prompt("Enter your hero name: "),
		origin: prompt("Enter origin: "),
		description: prompt("Enter description: "),
		skills: function() {
			console.log("Skills are " + firstSkill + ", " + secondSkill	+ ", and " + thirdSkill )
	}

}

console.log(hero)


